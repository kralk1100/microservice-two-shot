 Wardrobify
![Main Page](/uploads/2c8ae09e5b187b05a15d802f01cf2df3/Main.JPG)
![Adding a shoe](/uploads/5f5ae6bc29830dec58ccc71861b1b7b5/Add_shoe.JPG)
![Adding a hat](/uploads/a1e83fe5669bb31dadf32b4c082b7d8a/Add_hat.JPG)
![Hats](/uploads/81cb3262640115dfae96f916bc41da52/Hats.JPG)
Team:

* Jonathan Roman - Hats
* Sarah Ahn - Shoes

## Design

We used Django for the back-end and React for the front-end. We used RESTful APIs to get a list of shoes/hats, create new shoes/hats, and delete shoes/hats. The urls route us to our views which is similar to the middle ground between urls and models.

## Shoes microservice

The models for the Shoes microservice are Shoe and BinVO. We used poller to pull Wardrobe API data to our microservice.

## Hats microservice

Location has closet name, section number, and shelf number. Hat model has properties "fabric", "style", "color", "picture", "location". Encoders were used for views
