import React from "react";

class HatsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hats: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8090/api/hats/');
        const response = await fetch(url);
        if (response.ok) {
            const data =await response.json();
            this.setState({hats:data.hats});
        }
    }

    async handleDelete(event) {
        const hatsUrl = `http://localhost:8090/api/hats/${event}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const hatResponse = await fetch(hatsUrl, fetchConfig);
        if (hatResponse.ok) {
            this.setState ({hats: this.state.hats.filter(hat => hat.id != event)})
        }
        else {
            throw new Error ("Error");
        }
    }

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Style</th>
                            <th>Location</th>
                            <th>Fabric</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.hats.map(hat => {
                            return (
                                <tr key={hat.id}>
                                <td>{hat.style}</td>
                                <td>{hat.location}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.color}</td>
                                <td><img src={hat.picture} className = 'pic-thumbnail' width="120px" height = "120px" /></td>
                                <td><button className="btn btn-danger" onClick={() => this.handleDelete(hat.id)}>Delete</button></td>
                                </tr>
                            );
                            })}
                    </tbody>
                </table>
            </div>
        );
                        };
                    }

export default HatsList;
